all: clock alarm display set-alarm input_gpio grandfather_clock flags monk log tick mpd_client


clock:
	scp ./alarm-clock.py alarum:~/

alarm:
	scp ./alarm.py alarum:~/

display:
	scp ./display.py alarum:~/

set-alarm:
	scp ./set-alarm.sh alarum:~/

input_gpio:
	scp ./input_gpio.py alarum:~/

grandfather_clock:
	scp ./grandfather_clock.py alarum:~/

flags:
	scp ./flags.py alarum:~/
monk:
	scp ./monk.py alarum:~/

testdigits:
	scp ./testdigits.py alarum:~/

log:
	scp ./log.py alarum:~/

tick:
	scp ./tick.py alarum:~/

mpd_client:
	scp ./mpd_client.py alarum:~/
