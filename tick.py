import alarm
import flags
from display import leds
tick_volume = 35


def start_tick():
    """Set MPD playlist to 'tick' and then play 2s tick on repeat"""
    leds['tick'] = 1
    alarm.client.clear()
    alarm.client.load('tick')
    alarm.client.repeat('1')
    alarm.client.setvol(tick_volume)
    alarm.client.play()
    flags.TickFlag = True
    flags.TickOn = True


def stop_tick():
    """Rather confusingly, this does not *disable* ticking, for that you
need 'disable_tick'.  This should be addressed"""
    alarm.client.stop()
    alarm.client.repeat('0')
    flags.TickOn = False


def disable_tick():
    """Disable ticking"""
    flags.TickFlag = False
    stop_tick()
    leds['tick'] = 0
