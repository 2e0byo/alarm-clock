#!/bin/bash
set +x
set -e

h=19
a=9
b=15
c=5
d=21
e=14
f=2
g=20
one=6
two=18
three=17
four=7

function gl {
    $(./gpiocontrol.sh $1 out 0)
}

function gh {
    $(./gpiocontrol.sh $1 out 1)
}

function reset {

    gh $a
    gh $b
    gh $c
    gh $d
    gh $e
    gh $f
    gh $g
    gh $h
}

reset

    $(./gpiocontrol.sh $one out 0)
    $(./gpiocontrol.sh $three out 0)
    $(./gpiocontrol.sh $two out 0)
    $(./gpiocontrol.sh $four out 0)


gh $one

gl $a
sleep 1
gl $b
sleep 1
gl $c
sleep 1
gl $d
sleep 1
gl $e
sleep 1
gl $f
sleep 1
gl $g
sleep 1
gl $h
sleep 1
gl $one

reset

gh $two
gl $a
sleep 1
gl $b
sleep 1
gl $c
sleep 1
gl $d
sleep 1
gl $e
sleep 1
gl $f
sleep 1
gl $g
sleep 1
gl $h
sleep 1
gl $two
reset

gh $three
gl $a
sleep 1
gl $b
sleep 1
gl $c
sleep 1
gl $d
sleep 1
gl $e
sleep 1
gl $f
sleep 1
gl $g
sleep 1
gl $h
sleep 1
gl $three
reset


gh $four
gl $a
sleep 1
gl $b
sleep 1
gl $c
sleep 1
gl $d
sleep 1
gl $e
sleep 1
gl $f
sleep 1
gl $g
sleep 1
gl $h
sleep 1
reset
gl $four
