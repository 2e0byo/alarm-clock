#!/bin/bash

# pulled from alarm.sh to setup alarms on router.

# argument is string to be passed to date

# yn prompt
function yn_prompt() {
    read -p "$1 ([y]es or [N]o): "
    case $(echo $REPLY | tr '[A-Z]' '[a-z]') in
        y|yes) echo "yes" ;;
        *)     echo "no" ;;
    esac
}

# human_time="$1"
noisy="true"

while [ "$#" -gt 0 ]; do
  case "$1" in
    -q) noisy="false"; shift 1;;
    -*) echo "unknown option: $1" >&2; exit 1;;
    *) human_time="$1"; shift 1;;
  esac
done

if [ -z "$human_time" ]; then
    echo 'no time input'
    exit 1
fi


# saftey check the time-gap
epoch=$(/usr/bin/date -d "$human_time" +'%s') # epoch seconds
diff=$(echo "$epoch - $(/usr/bin/date '+%s') / 1" | bc ) 

if (( $diff <= 0 ))
then
    echo "proposed alarm is in the past, moving forward by 1 day"
    # workaround for timezone problems
   
    epoch=$(/usr/bin/date -d "$(/usr/bin/date '+%D' -d $human_time) + 1 day $(/usr/bin/date '+%H:%M' -d $human_time)" +'%s')
    
    diff=$(echo "$epoch - $(/usr/bin/date '+%s') / 1" | bc ) 
fi
    hours=$(($diff/3600))	# convert to hours


if (( $hours > 9 ))
then
    if [[ "no" == $(yn_prompt "Time to alarm is $hours hours. Continue? ") ]]
    then
        echo "try again...."
	exit
    fi
fi

echo "alarm set for $(/usr/bin/date -d @$epoch) in $hours hours." 


#echo -e '\n' >> /opt/alarms
s=$(/usr/bin/date "-d @$epoch" '+%A %d %B %Y %H:%M:%S')
if [ $noisy = "true" ]; then
    echo $s noisy >> /opt/alarms
else
    echo $s >> /opt/alarms
fi
