#!/bin/bash

# pulled from alarm.sh to setup alarms on router.

# argument is string to be passed to date

# yn prompt
function yn_prompt() {
    read -p "$1 ([y]es or [N]o): "
    case $(echo $REPLY | tr '[A-Z]' '[a-z]') in
        y|yes) echo "yes" ;;
        *)     echo "no" ;;
    esac
}

human_time="$1"

# saftey check the time-gap
epoch=$(/usr/bin/date -d "$human_time" +'%s') # epoch seconds
diff=$(echo "$epoch - $(/usr/bin/date '+%s') / 1" | bc ) 

if (( $diff <= 0 ))
then
    echo "proposed wakeup is in the past, moving forward by 1 day"
    # workaround for timezone problems
   
    epoch=$(/usr/bin/date -d "$(/usr/bin/date '+%D' -d $human_time) + 1 day $(/usr/bin/date '+%H:%M' -d $human_time)" +'%s')
    
    diff=$(echo "$epoch - $(/usr/bin/date '+%s') / 1" | bc ) 
fi
    hours=$(($diff/3600))	# convert to hours


if (( $hours > 9 ))
then
    if [[ "no" == $(yn_prompt "Time to alarm is $diff hours. Continue? ") ]]
    then
        echo "try again...."
	exit
    fi
fi

echo "waking up at $(/usr/bin/date -d @$epoch) in $hours hours." 

touch /tmp/.alarm-set

sleep $diff

mpc volume 0 
mpc play
rm /tmp/.alarm-set
n=0
while [ $n -le '50' ]; do
	mpc volume +2
	n=$(($n + 1))
	sleep 1
done

