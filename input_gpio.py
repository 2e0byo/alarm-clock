#!/usr/bin/python
from time import sleep
from log import log, test_print
import flags

gpio_poll_interval = 0.1  # seconds
long_press_timeout = 1  # seconds
left_button_flag = False
right_button_flag = False
both_buttons_function_called_flag = False


def gpio_status(gpio):
    with open('/sys/kernel/debug/gpio', "r") as f:
        for line in f:
            if 'gpio-' + str(gpio) in line:
                if 'hi' in line:
                    return (1)
                elif 'lo' in line:
                    return (0)
                else:
                    raise IOError(
                        'unable to determine state from kernel debug info')


def watch_gpio_change(gpio):  # returns when changes.
    while 1:
        try:
            a = gpio_status(gpio)
            sleep(gpio_poll_interval)
            if a != gpio_status(gpio):
                return
        except IOError as error:
            log(repr(error))
            pass  # why not


def watch_gpio_absolute(gpio, level):  # level is 1 or 0
    while 1:
        try:
            if gpio_status(gpio) == level:
                return
        except IOError as error:
            log(repr(error))
            pass  # why not
        sleep(gpio_poll_interval)


def watch_gpio_absolute_timeout(gpio, level, timeout,
                                other_button_flag):  # level is 1 or 0
    """Poll the marked gpio untill either 1. it reaches level (1 or 0), or
    2. other_button_flag is set (other button pressed) or 3. it times
    out.  Return True if timed out, 'other' if other button press
    detected and False otherwise

    """
    rtime = 0
    while rtime < timeout:
        if other_button_flag is True:
            return 'other'
        try:
            if gpio_status(gpio) == level:
                return False
        except IOError as error:
            log(repr(error))
            pass  # why not
        sleep(gpio_poll_interval)
        rtime += gpio_poll_interval
    return True


def left_watch_long_short_loop():
    """Watch left button for change in state, and then watch to see if
held down long enough to timeout or if released.  If 'short' press
detected call left_button_short, else if 'long' call
left_button_long"""
    global long_press_timeout, left_button_flag
    global right_button_flag, both_buttons_function_called_flag

    while 1:
        left_button_flag = False
        watch_gpio_absolute(504, 0)
        left_button_flag = True
        event = watch_gpio_absolute_timeout(504, 1, long_press_timeout,
                                            right_button_flag)
        if event is True:
            flags.functions['left_button_long']()
        elif event is 'other':  # other button pressed
            if both_buttons_function_called_flag is False:
                flags.functions['both_buttons']()
                both_buttons_function_called_flag = True
            sleep(2*gpio_poll_interval)
            right_button_flag = False
            left_button_flag = False
            both_buttons_function_called_flag = False
            test_print('left returning')
        elif right_button_flag is False:
            flags.functions['left_button_short']()
            test_print('left button short')

        # in case we timed out---prevent re-triggering
        watch_gpio_absolute(504, 1)


def right_watch_long_short_loop():
    """Watch right button for change in state, and then watch to see if
held down long enough to timeout or if released.  If 'short' press
detected call right_button_short, else if 'long' call
right_button_long"""
    global long_press_timeout, left_button_flag
    global right_button_flag, both_buttons_function_called_flag

    while 1:
        right_button_flag = False
        watch_gpio_absolute(503, 0)
        right_button_flag = True  # pressed

        event = watch_gpio_absolute_timeout(503, 1, long_press_timeout,
                                            left_button_flag)
        if event is True:
            flags.functions['right_button_long']()
        elif event is 'other':  # other button pressed
            if both_buttons_function_called_flag is False:
                flags.functions['both_buttons']()
                both_buttons_function_called_flag = True
            sleep(2*gpio_poll_interval)
            right_button_flag = False
            left_button_flag = False
            both_buttons_function_called_flag = False
            test_print('right returning')
        elif left_button_flag is False:
            flags.functions['right_button_short']()
            test_print('right button short')
        # in case we timed out---prevent re-triggering
        watch_gpio_absolute(503, 1)
