#!/usr/bin/python
from datetime import datetime, timedelta
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
from os import path

alarmfile = '/opt/alarms'


def prompt(message, y, n):  # case-insensitive
    y = y.lower()
    n = n.lower()
    while 1:
        choice = raw_input(message + ' ').lower()
        if choice == y or choice == n:
            break
        else:
            print('Illegal input, try again')
    if choice == y:
        return True
    else:
        return False


def set_alarm():
    global alarmfile
    while 1:
        try:
            proposed = raw_input('Enter time to alarm or alarm time ')
            at = parse(proposed)
        except ValueError:  # try relative
            if '+' in proposed:
                words = proposed.split(' ')
                p = {
                    'months': 0,
                    'weeks': 0,
                    'days': 0,
                    'hours': 0,
                    'minutes': 0,
                    'seconds': 0
                }
                for i in [
                        'months', 'weeks', 'days', 'hours', 'minutes',
                        'seconds'
                ]:
                    # allow for singular form
                    words = [
                        x + 's' if x == i.strip('s') else x for x in words
                    ]
                    if i in words:
                        try:
                            p[i] = float(words[words.index(i) - 1])
                            print('detected ' + i + ' as ' + str(p[i]))
                        except ValueError:
                            print('detected ' + i + ' but could not parse')
                            continue

                at = datetime.now() + relativedelta(
                    months=p['months'],
                    weeks=p['weeks'],
                    days=p['days'],
                    hours=p['hours'],
                    minutes=p['minutes'],
                    seconds=p['seconds'])
                if at == timedelta(minutes=0):
                    print(
                        'error, unable to determine future time, try again..')
                    continue
            else:
                print('illegal input, try again...')
                continue

        # check delta is sensible
        difference = at - datetime.now()
        if difference <= timedelta(minutes=0):
            print('proposed wakeup is in the past, moving forward by 1 day')
            at = at + relativedelta(days=1)
        if difference > timedelta(hours=9):
            if prompt('Time to alarm is ' + str(difference.seconds / 3600) +
                      " hours.  Continue? [y/n]", "y", "n") is False:
                print('try again....')
                continue

        # todo: add modular calculation of hours and minutes
        if prompt('alarm set for ' + at.strftime('%H:%M:%S') +
                  ' continue? [y/n]', 'y', 'n') is True:
            break

    # with open('/tmp/.alarm-set','w') as f:
    #     f.write('alarm set')

    if path.isfile(alarmfile):
        with open(alarmfile, 'r') as f:
            alarms = f.readlines()
        for i in xrange(len(alarms)):
            if not alarms[i].endswith('\n'):
                alarms[i] = alarms[i] + '\n'
    else:
        alarms = []

    if len(alarms) != 0:
        print('currently alarms are set for:')
        for alarm in alarms:
            print(alarm)

    alarms.append(at.strftime("%A %d %B %Y %H:%M:%S"))
    with open(alarmfile, 'w') as f:
        f.writelines(alarms)


set_alarm()

# todo: break into more functions, and then add ability to pass input via stdin
# then include in 'manage alarms' script
