#!/usr/bin/python
import csv
import signal
import sys
import traceback
from datetime import datetime, timedelta
from dateutil.parser import parse
from os import getpid, path, stat, system
from threading import Thread
from time import sleep, strftime, time

import alarm
import mpd_client
import grandfather_clock
import input_gpio
import flags
import monk
import tick
from display import init_gpio, leds, print_right, toggle_blank_display
from log import log, logf, test_print

# hardcoded variables
alarmfile = '/opt/alarms'
alarms = []
elapsed = 0
monkfile = '/opt/office.csv'
monk_schedule = {}
file_modification_dates = {}
interactive_string = ""  # prompt for interactive mode
global_alarm_list = []
global_noisy_list = []
current_mode = ""
last_mode = ""
pre_alarm_show_mode = ""
current_alarm_index = 0
alarm_file_lock = False

log.debug_mode = False


def do_nothing():
    pass


# --------------------------------------------------------------------
# Dump/Reload Config
# --------------------------------------------------------------------


def dump_config():
    # flags
    logf('/tmp/dump', 'flags:')
    for i in [i for i in vars(flags) if not i.startswith('__')]:
        logf('/tmp/dump', [i, vars(flags)[i]])
    logf('/tmp/dump', 'locals:')
    logf('/tmp/dump', locals())
    logf('/tmp/dump', "globals:")
    logf('/tmp/dump', globals())


def signal_handler(signal, frame):
    dump_config()


# --------------------------------------------------------------------
# File Functions
# --------------------------------------------------------------------


def poll_file(path):  # relies on global dictionary
    try:
        modtime = stat(path)[8]
    except OSError:
        return False  # path doesn't exist
    try:
        oldmodtime = file_modification_dates[path]
    except KeyError:
        file_modification_dates[path] = modtime
        return True  # first time looking
    if modtime != oldmodtime:
        file_modification_dates[path] = modtime
        return True  # modified
    else:
        return False  # unmodified


# --------------------------------------------------------------------
# Interactive functions
# --------------------------------------------------------------------
def interactive_up():
    """Increment user_input by 1 if less than interactive_upper_limit and
redisplay"""
    if flags.user_input < flags.interactive_upper_limit:
        flags.user_input += 1
        interactive_display()


def interactive_down():
    """Decrement user_input by 1 if greater than interactive_lower_limit
and redisplay"""
    if flags.user_input > flags.interactive_lower_limit:
        flags.user_input -= 1
        interactive_display()


def interactive_exit():
    """Exit interactive input mode"""
    flags.interactive = False


def interactive_display():
    """Redisplay interative_string + current value of user_input"""
    global interactive_string
    leds['dots'] = 0
    print_right(6, interactive_string + str(flags.user_input).zfill(2))


def interactive_mode():
    """Enter interactive input mode"""
    new_mode('interactive')
    flags.async_display = True  # prevent clashes
    flags.interactive = True
    flags.functions["left_button_long"] = interactive_exit
    flags.functions["left_button_short"] = interactive_down
    flags.functions["right_button_short"] = interactive_up
    flags.functions["right_button_long"] = interactive_exit
    interactive_display()


def async_refresh_display():
    if flags.async_display is True:
        return  # async display in progress
    if flags.display_lock is True:  # display in progress
        sleep(0.2)
    flags.async_display = True
    flags.display_function()
    flags.async_display = False


def async_print_right(length, digits, timeout=0, attention=False, dots=True):
    """An asynchronous version of print_right with optional timeout which
optionally calls attention to itself by switching off (most of) the
leds for the duration of display. (We don't block the main loop
because that would contradict the philosophy of reliable chiming.  But
currently some of the 'check' functions (alarm and monk) set their
leds iff files exist (and not just 'if turned on').  This is a
feature, but it's side effect is that they come on during the display.
We could theoretically cure it by having an inhibit flag, but I prefer
to leave it as it is.  By default we assume display of a time and so
leave 'dots' on, but this can be avoided by setting dots=False

    """
    global leds

    if flags.display_lock is True:  # display in progress
        sleep(0.2)
    async = flags.async_display
    flags.async_display = True
    current_leds = leds.copy()
    if attention is True:
        for led in leds:
            leds[led] = 0
    if dots is True:
        leds['dots'] = 1
    print_right(length, digits)
    print_right(length, digits)
    sleep(timeout)
    for i in leds:
        leds[i] = current_leds[i]
    flags.async_display = async


# --------------------------------------------------------------------
# Alarm Functions
# --------------------------------------------------------------------


def stop_alarm(snoozing=False):
    global elapsed
    elapsed = 0
    alarm.stop_running_alarm(snoozing)
    clock_mode()


def check_alarm():
    """If not inhibited by ongoing snooze or alarm, check for
alarms in alarmfile.  If alarm in progress check for elapsed snooze"""
    global alarms, elapsed
    ring = False  # don't initiate ring
    if flags.interactive is True:  # in interactive input mode
        return
    if flags.AlarmFlag is True:
        elapsed += 1
        if flags.snooze is not False:
            flags.snooze -= 1
            if flags.snooze != 0:
                return
            else:
                # print('snooze elapsed')
                ring = True

    if alarm_file_lock is True:
        return
    global alarmfile
    alarm_list = []
    if (path.isfile(alarmfile)) and (path.getsize(alarmfile) > 0):
        leds['alarm'] = 1
        modified = False
        with open(alarmfile, 'r') as f:
            alarms = f.readlines()
        for entry in alarms:
            try:
                noisy = False
                entry_time = entry
                if 'noisy' in entry:
                    entry_time = entry.replace('noisy', "")
                    noisy = True
                alarmtime = parse(entry_time.strip('\n'))
                if alarmtime not in alarm_list:
                    alarm_list.append(alarmtime)
                if datetime.now() - alarmtime >= timedelta(minutes=0):
                    flags.AlarmFlag = True
                    modified = True
                    ring = True
                    flags.noisy = noisy
                    alarms.remove(entry)
                    elapsed = 0  # reset alarm count
            except ValueError:
                pass
        if modified is True:
            with open(alarmfile, 'w') as f:
                for entry in alarms:
                    f.write(entry)
    else:
        leds['alarm'] = 0
    if ring is True:  # ring
        # print('ringing')
        alarm_mode()
        Thread(target=alarm.play_alarm()).start()
    global global_alarm_list
    alarm_list.sort()
    global_alarm_list = alarm_list


def set_snooze():
    """Interactively set a snooze alarm for min minutes into the future"""
    global interactive_string
    stop_alarm(snoozing=True)  # stop ringing---leaves us in clock_mode
    interactive_string = 'snze'
    interactive_mode()
    interactive_display()  # try to cure bad displaying

    #    timeout = 1500
    timeout = 300
    while (flags.interactive is True) and (timeout > 0):
        sleep(0.2)  # wait for user input
        timeout -= 1
    interactive_exit()  # in case timed out

    mins = flags.user_input
    if mins == 0:
        flags.snooze = 01  # trigger immediately, to prompt for
        # correction
    else:
        flags.snooze = mins * 60  # seconds
    alarm_mode()
    flags.AlarmFlag = True  # inhibit anything else in snooze!
    flags.async_display = False
    flags.display_function = display_snooze


def background_set_snooze():
    Thread(target=set_snooze).start()


def display_alarm():
    global elapsed
    print_right(6, strftime('%H%M') + str(elapsed // 60)[0::1].zfill(2))


def display_snooze():
    if flags.snooze >= 60:
        print_right(6, strftime('%H%M') + str(flags.snooze // 60).zfill(2))
    else:
        print_right(6, strftime('%H%M') + str(flags.snooze)[0::1].zfill(2))


# --------------------------------------------------------------------
# Show and edit alarm times
# --------------------------------------------------------------------


def alarm_show_mode():
    """Asynchronous mode.  Cycle through alarms.  Long press to edit, both
to exit.  Times out."""
    flags.async_display = True
    new_mode('alarm_show')
    flags.display_function = do_nothing
    leds['dots'] = 1
    flags.functions["left_button_long"] = edit_current_alarm
    flags.functions["left_button_short"] = show_previous_alarm
    flags.functions["right_button_short"] = show_next_alarm
    flags.functions["right_button_long"] = edit_current_alarm
    flags.functions["both_buttons"] = exit_alarm_show_mode


def show_edit_alarms():
    """must return quickly as blocks further button input"""
    global global_alarm_list
    global pre_alarm_show_mode, current_mode
    pre_alarm_show_mode = current_mode
    Thread(target=background_flash_alarm).start()


def background_flash_alarm():
    if len(global_alarm_list) == 0:
        async_print_right(6, 'no alr', timeout=2, attention=True, dots=False)
        return
    alarm_show_mode()
    show_nth_alarm(0)
    if flags.InteractiveAlarmFlag is False:
        exit_alarm_show_mode()
        test_print('auto returning flash')


def exit_alarm_show_mode():
    flags.InteractiveAlarmFlag = False
    global pre_alarm_show_mode
    switch_mode(pre_alarm_show_mode)
    flags.async_display = False


def show_nth_alarm(n, blocking=True):
    global global_alarm_list, current_alarm_index
    current_alarm_index = n
    if blocking is False:
        t = 0
    else:
        t = 2
    if len(global_alarm_list) == 0:
        async_print_right(6, 'no alr', timeout=t, attention=True, dots=False)
        return
    next_alarm = global_alarm_list[n]

    async_print_right(
        6,
        next_alarm.strftime('%H%M') + str(
            (next_alarm.date() - datetime.now().date()).days).zfill(2),
        timeout=t,
        attention=True)

    # functions to be called interactively.  From this point on
    # leaving requires manual selection.  could we have them each
    # restart a background timer to time out every time they are
    # called?  And then would automatically return to clock?


def show_next_alarm():
    flags.InteractiveAlarmFlag = True
    global global_alarm_list, current_alarm_index
    i = current_alarm_index + 1
    if i >= len(global_alarm_list):
        i = 0  # roll over
    show_nth_alarm(i, blocking=False)


def show_previous_alarm():
    flags.InteractiveAlarmFlag = True
    global global_alarm_list, current_alarm_index
    i = current_alarm_index - 1
    if i < 0:
        i = len(global_alarm_list) - 1  # roll over
    show_nth_alarm(i, blocking=False)


def edit_current_alarm():
    flags.InteractiveAlarmFlag = True
    # let us know we got there, into dangerous ground
    async_print_right(6,'      ')
    sleep(0.1)                  # get attention
    alarm_edit_mode()
    show_nth_alarm(current_alarm_index, blocking=False)


# --------------------------------------------------------------------
# Edit Alarm Functions
# --------------------------------------------------------------------


def alarm_edit_mode():
    new_mode('alarm_edit')
    flags.async_display = True
    flags.display_function = do_nothing
    leds['dots'] = 1
    flags.functions["left_button_long"] = delete_current_alarm
    flags.functions["left_button_short"] = alarm_down_minute
    flags.functions["right_button_short"] = alarm_up_minute
    flags.functions["right_button_long"] = alarm_up_hour
    flags.functions["both_buttons"] = exit_alarm_edit_mode


def exit_alarm_edit_mode():
    flags.InteractiveAlarmFlag = False
    alarm_show_mode()
    async_print_right(6, '      ')
    sleep(0.1)                  # get attention
    show_nth_alarm(0, blocking=False)  # move to beginning of list


def alarm_up_minute():
    """increment current alarm by 1 minute"""
    global current_alarm_index
    alarm_up_down(current_alarm_index, 1)


def alarm_up_hour():
    """increment current alarm by 1 hour"""
    global current_alarm_index
    alarm_up_down(current_alarm_index, 60)


def delete_current_alarm():
    """delete current alarm from global_alarm_list, re-write file, exit
alarm_edit_mode"""
    global current_alarm_index, global_alarm_list
    del (global_alarm_list[current_alarm_index])
    global alarm_file_lock, alarmfile

    alarm_file_lock = True  # nobody else is to edit anything...
    with open(alarmfile, 'w') as f:
        for entry in global_alarm_list:
            f.write(entry.strftime("%A %d %B %Y %H:%M:%S") + "\n")
    alarm_file_lock = False
    exit_alarm_edit_mode()


def alarm_down_minute():
    """decrement current alarm by 1 minute"""
    alarm_up_down(current_alarm_index, -1)


def alarm_up_down(alarm_index, mins):
    """Lock the alarm file, increment the alarm in global_alarm_list at
alarm_index by mins minutes (if negative decrement) and then wholly
replace the file with the contents of global_alarm_list.  Note this is
dangerous: it needs a timeout!  Note that using this will strip
'noisy' flags (because I'm currently sufficiently tired it seems
nontrivial to cope with them).  When finished redisplay current alarm
time.

    """
    global alarm_file_lock, alarmfile
    global global_alarm_list
    alarm_file_lock = True  # nobody else is to edit anything...
    global_alarm_list[alarm_index] += timedelta(minutes=mins)
    with open(alarmfile, 'w') as f:
        for entry in global_alarm_list:
            f.write(entry.strftime("%A %d %B %Y %H:%M:%S")+ "\n")
    alarm_file_lock = False
    show_nth_alarm(alarm_index, blocking=False)


# --------------------------------------------------------------------
# Chime Functions
# --------------------------------------------------------------------


def check_chime():
    if leds['chime'] == 0:  # Chime off
        return
    if flags.interactive is True:  # in interactive input mode
        return
    if flags.ChimeFlag is True:  # already chiming or waiting for
        # minute to elapse
        return
    if flags.AlarmFlag is True:  # running/snoozed alarm
        return
    if flags.MonkFlag is True:  # running toll
        return

    nowm = int(strftime('%M'))
    nowh = strftime('%I')

    if (nowm % 15 == 0):
        if leds['tick'] == 1:
            stop_tick()
        flags.ChimeFlag = True  # moved to prevent race condition
        Thread(
            target=grandfather_clock.chime, args=(nowm, nowh)).start()  # chime


def stop_chime():
    grandfather_clock.stop_chime()


def toggle_chime():
    if leds['chime'] == 0:
        leds['chime'] = 1
        async_refresh_display()
    else:
        leds['chime'] = 0
        async_refresh_display()
        stop_chime()


# --------------------------------------------------------------------
# Monastery Bell Functions
# --------------------------------------------------------------------


def check_monk():
    if flags.MonkOn is False:
        return
    if flags.interactive is True:  # in interactive input mode
        return
    if flags.MonkFlag is True:  # chiming
        return
    if flags.AlarmFlag is True:
        return
    if flags.ChimeFlag is True:
        return

    global monkfile

    # check for updates to liturgy schedule

    if (path.isfile(monkfile)) and (path.getsize(monkfile) > 0):
        leds['monk'] = 1
        if (poll_file(monkfile) is True):
            with open(monkfile, 'r') as f:
                reader = csv.DictReader(f)
                for row in reader:
                    monk_schedule[row["day"]] = {
                        "officium lectionis": row["officium lectionis"],
                        "laudes matutinus": row["laudes matutinus"],
                        "tertia": row["tertia"],
                        "sextus": row["sextus"],
                        "nona": row["nona"],
                        "vesperae": row["vesperae"],
                        "completorium": row["completorium"]
                    }
    else:
        leds['monk'] = 0
        return  # don't used cached version if currently re-writing!

    # use ram version of schedule to check for an alarm
    for office in [
            "officium lectionis", "laudes matutinus", "tertia", "sextus",
            "nona", "vesperae", "completorium"
    ]:
        if monk_schedule[strftime("%A")][office] == strftime("%H:%M"):
            flags.MonkFlag = True  # add here to prevent race condition
            Thread(target=monk.toll, args=(office, )).start()
            return  # on the offchance two are scheduled on top of each
            # other, only chime the first


def stop_monk():
    monk.stop_toll()


def toggle_monk():
    if flags.MonkOn is True:
        flags.MonkOn = False
        async_refresh_display()
        stop_monk()
    else:
        flags.MonkOn = True
        async_refresh_display()


# --------------------------------------------------------------------
# Tick Functions
# --------------------------------------------------------------------


def check_tick():
    """Check if tick has been stopped by something else and should be
re-enabled, and if so start it"""
    if flags.MonkFlag is True:
        return
    if flags.AlarmFlag is True:
        return
    if flags.ChimeFlag is True:
        return
    if flags.TickFlag is True and flags.TickOn is False:
        # print 'restarting tick'
        start_tick()


def start_tick():
    tick.start_tick()
    async_refresh_display()


def stop_tick():
    tick.stop_tick()
    async_refresh_display()


def disable_tick():
    tick.disable_tick()
    async_refresh_display()


def toggle_tick():
    if leds['tick'] == 0:
        start_tick()
    else:
        disable_tick()


# --------------------------------------------------------------------
# Modes
# --------------------------------------------------------------------


def new_mode(mode):
    """update current and last mode"""
    global last_mode
    global current_mode
    last_mode = current_mode
    current_mode = mode


def switch_last_mode():
    """Exit current mode and go back to the last one"""
    global last_mode
    global current_mode
    last_mode = current_mode
    switch_mode(current_mode)


def switch_mode(mode):
    if mode == 'clock':
        clock_mode()
    elif mode == 'alarm':
        alarm_mode()
    elif mode == 'interactive':
        interactive_mode()
    elif mode == 'alarm_edit':
        alarm_edit_mode()
    elif mode == 'alarm_show':
        alarm_show_mode()


def clock_mode():
    new_mode('clock')
    flags.display_function = display_time
    leds['dots'] = 1
    flags.functions["left_button_long"] = toggle_monk
    flags.functions["left_button_short"] = toggle_chime
    flags.functions["right_button_short"] = toggle_tick
    flags.functions["right_button_long"] = toggle_blank_display
    flags.functions["both_buttons"] = show_edit_alarms


def alarm_mode():
    new_mode('alarm')
    stop_chime()
    stop_tick()
    flags.display_function = display_alarm
    leds["dots"] = 1
    flags.functions["left_button_long"] = background_set_snooze
    flags.functions["left_button_short"] = background_set_snooze
    flags.functions["right_button_short"] = stop_alarm
    flags.functions["right_button_long"] = stop_alarm
    flags.functions["both_buttons"] = show_edit_alarms


def display_time():
    print_right(6, strftime('%H%M%S'))


# --------------------------------------------------------------------
# Main Loop
# --------------------------------------------------------------------


def clock_loop():
    while 1:
        start = time()
        if flags.async_display is False:
            flags.display_lock = True
            flags.display_function()
            flags.display_lock = False
        # order of preferece
        check_alarm()
        check_monk()
        check_chime()
        check_tick()
        try:
            sleep(1 + start - time())
        except:  # we get a bunch of errors.  Sometime analyse them
            # and clean this up.
            print('Skipped.  If this keeps happening, check system load')
            system(
                "logger 'Alarm clock skipped, if this keeps happening, check system\
load'")


#        raise ValueError("Testing Error") # testing

# --------------------------------------------------------------------
# Execution Starts Here
# --------------------------------------------------------------------

# prevent multiple loading (can cause conflicts)
lockfile = "/tmp/alarm-lock"
if path.isfile(lockfile):
    log('already running')
    log('if you mean to restart, please remove ' + lockfile)
    sys.exit(1)

with open(lockfile, 'w') as f:
    log('writing lock file')
    f.write(str(getpid()) + '\n')
    f.write(strftime("%D   %H:%M:%S") + '\n')

grandfather_clock.set_volume(flags.max_hardware_volume)

try:  # catch all errors, so we can warn the
    # user that clock has stopped

    init_gpio()
    mpd_client.mpd_alive_thread()

    Thread(target=input_gpio.left_watch_long_short_loop).start()
    Thread(target=input_gpio.right_watch_long_short_loop).start()
    log('started clock...')

    signal.signal(signal.SIGUSR1, signal_handler)

    clock_mode()
    toggle_chime()
    toggle_monk()
    toggle_tick()
    clock_loop()

except Exception as e:
    print_right(6, 'err   ')
    alarm.fallback_tones()
    log('Exception!  This shouldn\'t happen....')
    log(e)
    traceback.print_exc(file=sys.stdout)
    sys.exit(1)
