#!/usr/bin/python
import display
import string
import time

delay = 0.2

display.init_gpio()

# test all digits

for digit in [
        display.one, display.two, display.three, display.four, display.five,
        display.six
]:
    for i in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]:
        display.write_digit(digit, i)
        time.sleep(delay)

# test font

for letter in string.ascii_lowercase:
    print(letter)
    display.write_digit(display.six, letter)
    raw_input('press enter')
