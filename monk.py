# functions to toll
from mpd_client import play, stop

import flags
import tick

office_bells = {
    "officium lectionis": 'lesser',
    "laudes matutinus": 'greater',
    "tertia": 'lesser',
    "sextus": 'lesser',
    "nona": 'lesser',
    "vesperae": 'greater',
    "completorium": 'lesser'
}

bells_dir = "/home/john/monastery-bell/"


def toll(office):
    global toll_process
    flags.MonkFlag = True  # chiming
    # stop tick
    if flags.TickFlag is True:
        tick.stop_tick()
    # chime
    global office_bells
    play(office_bells[office], 0, flags.monk_volume)

    if flags.TickFlag is True:
        tick.start_tick()
    flags.MonkFlag = False


def stop_toll():
    if flags.MonkFlag is True:
        stop()
    flags.MonkFlag = False
