from threading import Thread
from time import sleep
from subprocess import call
from display import unblank_display
from mpd_client import client, ConnectionError, reconnect, stop
from mpd import CommandError
import flags
from display import leds
from tick import stop_tick, start_tick

alarm_stop = False
snooze = False  # a proper stop
alarm_running = False


def stop_running_alarm(snoozing=False):
    """Test if alarm is running, and if so, stop it; if a hard stop,
cancel any unelapsed snooze."""
    global alarm_stop, snooze
    snooze = snoozing
    if alarm_running is True:
        alarm_stop = True
    if snoozing is False:       # hard stop
        flags.snooze = False


def alarm_watchdog(tick, random, planned, planned_length):
    # watch for 'stop' flag, whilst increasing volume, then just waiting
    global alarm_running
    alarm_running = True
    global alarm_stop, snooze
    alarm_stop = False
    # can be left dangling if I'm an idiot.  This is belt-and-braces to
    # catch something daft like setting 'alarm_stop' manually (without
    # calling 'stop_running_alarm'

    volume = 0
    while volume <= 98:
        volume += 2
        client.setvol(volume)
        sleep(1)
        if alarm_stop is True:
            # print('stopped by alarm_stop')
            break
    timeout = 13500  # 45*60/0.2
    error_count = 0
    while (alarm_stop is False) and (timeout > 0):
        # print('waiting')
        sleep(.2)
        timeout -= 1
        try:
            if (timeout % 150 == 0) and (client.status()['state'] !=
                                         "play"):  # only every 30 seconds!
                fallback_tones()  # this is drastic.  Better to wake
                # up, though.
        except (ConnectionError, KeyError):
            reconnect()
            error_count += 1
            if error_count > 2:
                fallback_tones()  # give up

    alarm_stop = False
    try:
        stop()
    except ConnectionError:
        reconnect()
        stop()
    client.random(random)
    alarm_running = False
    if snooze is True:  # leave everything alone
        return

    # the point of the 'planned' list is to choose the wakeup song.
    # So we opt for forcing the same one every time if we snooze.
    if planned is True:
        for i in range(planned_length):
            client.playlistdelete('wakeup', 0)  # clear wakeup playlist
    if tick is True:
        start_tick()
    if flags.noisy is True:
        start_tick()
        leds['chime'] = 1  # sometime standardise this mess!
        flags.MonkOn = True
    flags.AlarmFlag = False


def play_alarm():
    """Turn display on and play alarm.

planned = tracks pre-added to 'wakeup' playlist (which take preference
over randomly selected tracks from 'alarm' playlist.

    """
    # print('reached play_alarm')
    flags.AlarmFlag = True
    unblank_display()
    if flags.TickFlag is True:
        tick = True
        stop_tick()
    else:
        tick = False
    client.setvol(0)

    random = client.status()['random']
    planned = False
    try:
        planned_length = len(client.listplaylist('wakeup'))
        if planned_length > 0:
            planned = True
            client.random('0')
            client.clear()
            client.load('wakeup')
            client.load(
                'alarm')  # to make sure we don't run out before waking up!
    except CommandError:  # wakeup likely doesn't exist
        planned = False  # to rub the point in for later edits...
    if planned is False:
        client.random('1')
        client.clear()
        client.load('alarm')
        client.next()
    client.play()
    # print('playing....')
    # print('status: ', client.status()['state'])
    Thread(
        target=alarm_watchdog, args=(tick, random, planned,
                                     planned_length)).start()


def fallback_tones():
    """Play loud annoying tones with speaker-test in case everything goes
wrong"""
    # prevent hogging sound card: you're going to need to reboot after this
    call(["pkill", "-f", "mpd"])
    # lots of loud noise for a few seconds
    call(["speaker-test", "-l", "2"])
