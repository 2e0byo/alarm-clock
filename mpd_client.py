from threading import Thread
from time import sleep

from mpd import MPDClient, ConnectionError
client = MPDClient()


def reconnect():
    client.connect("localhost", 6600)


reconnect()                     # set up first connection


def mpd_alive_thread():
    Thread(target=mpd_alive).start()


def mpd_alive():
    while 1:
        sleep(55)
        try:
            client.ping()
        except:  # naughty naughty, but anyhow....
            reconnect()


def play(playlist, item, volume):
    """stop current playback, erase dynamic playlist, load element 'item'
from new playlist, and play it. Sleep for the duration of the song,
and then poll if needed (as we can mis-estimate by up to 1s).  Then
force stop and exit to avoid getting stuck (e.g. you paused it).

    """
    client.stop()
    client.clear()
    client.repeat('0')
    client.setvol(volume)
    client.load(playlist, item)
    client.play()
    sleep(int(client.currentsong()['time']))
    if client.status()['state'] == 'play':
        sleep(1)


def stop():
    """fade out and gracefully stop"""
    volume = int(client.status()['volume'])
    while volume > 4:
        volume -= 5
        client.setvol(volume)
        sleep(0.1)
    client.setvol(0)
    client.stop()
