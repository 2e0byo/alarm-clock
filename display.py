import flags
# outputs
h = 499
a = 489
b = 495
c = 485
d = 501
e = 494
f = 482
g = 500
one = 486
two = 498
three = 497
four = 487
six = 'green:usb'
five = "green:dsl"
# base=480

leds = {'dots': 1, 'tick': 0, 'chime': 0, 'monk': 0, 'alarm': 0, 'na': 0}
led_per_digits = {
    one: 'dots',
    two: 'tick',
    three: 'chime',
    four: 'monk',
    five: 'alarm',
    six: 'na'
}

# --------------------------------------------------------------------
# 7-seg font
# --------------------------------------------------------------------

font = {
    '0': [a, b, c, d, e, f],
    '1': [b, c],
    '2': [a, b, d, e, g],
    '3': [a, b, g, c, d],
    '4': [f, g, b, c],
    '5': [a, f, g, c, d],
    '6': [f, e, d, c, g, a],
    '7': [a, b, c],
    '8': [a, b, c, d, e, f, g],
    '9': [a, b, f, g, c, d],
    'a': [e, f, a, b, c, g],
    'b': [f, e, d, g, c],
    'c': [g, e, d],
    'd': [d, e, g, c, b],
    'e': [a, f, g, e, d],
    'f': [a, f, g, e],
    'g': [a, f, g, b, c, d],
    'h': [f, e, g, c],
    'i': [f, e],
    'j': [b, c, d],
    'k': [f, e, g],
    'l': [f, e, d],
    'm': [e, g, c],
    'n': [e, g, c],
    'o': [g, e, c, d],
    'p': [f, a, b, g, e],
    'q': [f, a, b, g, c],
    'r': [e, g],
    's': [a, f, g, c, d],
    't': [a, f, e],
    'u': [e, d, c],
    'v': [e, d, c],
    'w': [e, d, c],
    'x': [g],
    'y': [f, g, b, c],
    'z': [a, b, g, e, d],
    ' ': [],
    '-': [d]
}

# --------------------------------------------------------------------
# init
# --------------------------------------------------------------------


def init_gpio():
    for i in [a, b, c, d, e, f, g, h, one, two, three, four]:
        try:
            with open('/sys/class/gpio/export', 'w') as out:
                out.write(str(i))
        except OSError:
            pass
        with open('/sys/class/gpio/gpio' + str(i) + '/direction', 'w') as out:
            out.write('out')


# --------------------------------------------------------------------
# Physical Interface
# --------------------------------------------------------------------


def set_gpio(pin, value):
    with open('/sys/class/gpio/gpio' + str(int(pin)) + '/value', 'w') as f:
        f.write(str(value))


def set_led(led, value):
    with open('/sys/class/leds/A4001N:' + led + '/brightness', 'w') as f:
        if value == 1:
            f.write('0')
        elif value == 0:
            f.write('100')


def reset_out(state):
    for i in [a, b, c, d, e, f, g, h]:
        set_gpio(i, str(state))


# --------------------------------------------------------------------
# Display Functions
# --------------------------------------------------------------------


def write_digit(digit, n):
    n = str(n)
    on = font[n]

    if digit in [one, two, three, four]:  # inverting buffer
        reset_out(1)
        if flags.DisplayBlanked is False:  # else just leave it blanked
            for segment in on:
                set_gpio(segment, 0)
            if leds[led_per_digits[digit]] == 0:  # flip logic
                set_gpio(h, 1)
            else:
                set_gpio(h, 0)

        set_gpio(digit, 1)
        set_gpio(digit, 0)

    elif digit in [five, six]:
        reset_out(0)
        if flags.DisplayBlanked is False:
            for segment in on:
                set_gpio(segment, 1)
            set_gpio(h, leds[led_per_digits[digit]])

        set_led(digit, 1)
        set_led(digit, 0)

    else:
        raise ValueError("Illegal digit supplied: " + str(digit))


def print_right(length, digits):
    """Print number right-justified, up to 6 digits.  'digits' must be indexable!'
Note that this currently also flushes the leds from the flags"""
    if flags.MonkOn is True:
        leds['monk'] = 1
    else:
        leds['monk'] = 0
    # Write somethiing similar for everything else
    # to avoid the inelegance of 'leds[n]' all the time
    rl = [six, five, four, three, two, one]
    digits = list(reversed(digits))
    for i in range(length):
        write_digit(rl[i], str(digits[i]))


def blank_display():
    # set_led('red:inet', 0)
    flags.DisplayBlanked = True


def unblank_display():
    # set_led('red:inet', 1)
    flags.DisplayBlanked = False


def toggle_blank_display():
    if flags.DisplayBlanked is True:
        unblank_display()
    else:
        blank_display()
