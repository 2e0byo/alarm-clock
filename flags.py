#!/usr/bin/python
ChimeFlag = False  # not chiming
ChimeOn = 'Unimplemented'
AlarmFlag = False  # not ringing
AlarmOn = 'Unimplemented'
MonkFlag = False  # no monks neither...
MonkOn = False
TickFlag = False  # ...nor is it a bomb
TickOn = False
DisplayBlanked = False
functions = {
    "left_button_long": "",
    "left_button_short": "",
    "right_button_long": "",
    "right_button_short": "",
    "both_buttons": ""
}
display_function = ""
snooze = False
interactive = False
user_input = 0
interactive_upper_limit = 99
interactive_lower_limit = 0
async_display = False
display_lock = False
max_hardware_volume = '60%'
chime_volume = 80
monk_volume = 100
# whether to restore all 'noisy' functions after alarm elapses
noisy = False
InteractiveAlarmFlag = False
